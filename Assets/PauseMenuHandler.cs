using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuHandler : MonoBehaviour
{
    Animator  pauseMenu;

    private void Start()
    {
        pauseMenu = GetComponentInChildren<Animator>();

    }
    public void PauseMenuOn()
    {
        TimeScaler tm = FindObjectOfType<TimeScaler>();
        tm.SlowTIme(0f);
        pauseMenu.SetBool("isOpen", true);
    }
    public void PauseMenuOff()
    {
        TimeScaler tm = FindObjectOfType<TimeScaler>();
        tm.NormalTIme();
        pauseMenu.SetBool("isOpen", false);


    }
}
