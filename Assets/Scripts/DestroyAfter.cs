using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    [SerializeField] float timeToDestroy = 2f;
    private void Awake()
    {
        Destroy(gameObject, timeToDestroy);
    }
}
