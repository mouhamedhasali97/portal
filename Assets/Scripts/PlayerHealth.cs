using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] int decreaseIntensity = 30;
    //[SerializeField] GameObject playerDeathEffect;
    [SerializeField] GameObject playerDeathEffect;
    [SerializeField] GameObject playerDamageEffect;
    [SerializeField] private float Fullhealth = 100;
    private float currentHealth;
    Animator myAnim;

    Light spot;
    Color spotLight;


    private void Start()
    {
        currentHealth = Fullhealth;
        spot = GetComponentInChildren<Light>();
        myAnim = GetComponentInChildren<Animator>();
        spot.intensity = Fullhealth;
        spotLight = spot.color;


    }
    public void AddDamage(float damage)
    {
        Instantiate(playerDamageEffect, transform.position, transform.rotation);

        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            StartCoroutine(Die());
        }
        if (spot.intensity <= 10) return;
        spot.intensity -= decreaseIntensity;
        

    }
    private void Update()
    {
        if(Time.timeScale >= 1)
        {
            spot.color = spotLight;
        }
        else
        {
            spot.color = Color.red;
        }
    }
    private IEnumerator Die()
    {
        myAnim.SetTrigger("Die");
        yield return new WaitForSeconds(1f);
        Instantiate(playerDeathEffect, transform.position, transform.rotation);
        Destroy(gameObject);

    }


}
