using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleController : MonoBehaviour
{

    [SerializeField] List<Button> btns = new List<Button>();
    private bool firstGuess, secondGuess;
    private int firstGuessIndex, secondGuessIndex;
    private Color firstGuessColor, secondGuessColor;

    public void PickColor()
    {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
        int selectedIndex = int.Parse(name);

        if (!firstGuess)
        {
            firstGuess = true;
            firstGuessIndex = selectedIndex;
            firstGuessColor = btns[firstGuessIndex].image.color;
        }
        else if (!secondGuess)
        {
            secondGuess = true;
            secondGuessIndex = selectedIndex;
            secondGuessColor = btns[secondGuessIndex].image.color;

            if (secondGuessColor == firstGuessColor)
            {
                Debug.Log("Colors Match");
                FindObjectOfType<DoorTrigger>().Solved();
            }
            else
            {
                Debug.Log("Colors Don't Match!");
                firstGuess = false;
                secondGuess = false;
            }
        }
    }
    
}
