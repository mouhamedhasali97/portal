using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaler : MonoBehaviour
{
    [SerializeField] GameObject Player;
    Animator anime;
    Collider col;

   public void FreezeTime()
   {
        anime = Player.GetComponentInChildren<Animator>();
        col = Player.GetComponent<CapsuleCollider>();
        Time.timeScale = 0.5f;
   }
   public void NormalTIme()
   {
        Time.timeScale = 1f;
   }
    public void SlowTIme(float Amount)
   {
        Time.timeScale =Amount;
   }
}

