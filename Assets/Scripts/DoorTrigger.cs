using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class DoorTrigger : MonoBehaviour
{
    [SerializeField] GameObject ActivateMenuButton;
    [SerializeField] GameObject PanelMenuButton;
    [SerializeField] DoorOpen Door;
    //int AgentsInRange = 0;
    private void OnTriggerEnter(Collider other)
    {
        ActivateMenuButton.SetActive(true);
        
        
    } 
    private void OnTriggerExit(Collider other)
    {
        ActivateMenuButton.SetActive(false);
        PanelMenuButton.SetActive(false);
        
    }
    public void Solved()
    {
        if (!Door.isOpen)
        {
            Door.Open(FindObjectOfType<Player>().transform.position);
        }
    }
    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.TryGetComponent<NavMeshAgent>(out NavMeshAgent agent))
    //    {
    //        AgentsInRange--;
    //        if (Door.isOpen && AgentsInRange ==0)
    //        {
    //            Door.Close();
    //        }
    //    }
    //}



    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.TryGetComponent<NavMeshAgent>(out NavMeshAgent agent))
    //    {
    //        AgentsInRange++;
    //        if (!Door.isOpen)
    //        {
    //            Door.Open(other.transform.position);
    //        }
    //    }
    //}
}
