using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleFixedMovement : MonoBehaviour
{
    [SerializeField]
    float Speed = 4.5f;
    [SerializeField] float Range = 4f;
    float startingY;
    int dir = 1;
    void Start()
    {
       startingY = transform.position.y;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        transform.Translate( Vector3.up * Speed * Time.deltaTime*dir);
        if (transform.position.y > startingY + Range || transform.position.y < startingY + Range)
            dir *= -1;

    }
}
