using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{   [SerializeField] float damageToAdd = 10f;
    //[SerializeField] GameObject explosionVfx;
    private void OnTriggerEnter(Collider other)
    {
       // Instantiate(explosionVfx, transform.position, transform.rotation);
        if (other.tag == "Enemy")
        {
            EnemyHealth enemyheal = other.gameObject.GetComponent<EnemyHealth>();
            enemyheal.AddDamage(damageToAdd);
        }
    }  
    private void OnTriggerStay(Collider other)
    {
       // Instantiate(explosionVfx, transform.position, transform.rotation);
        if (other.tag == "Enemy")
        {
            EnemyHealth enemyheal = other.gameObject.GetComponent<EnemyHealth>();
            enemyheal.AddDamage(damageToAdd);
        }
    }
}
