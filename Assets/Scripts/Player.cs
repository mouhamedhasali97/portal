using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{

    [SerializeField] Camera mainCamera;
    [SerializeField] float speed = 6.89f;
    private Vector2 move, mouseLook, joyStickLook, attack;
    private Vector3 rotationTarget;
    public bool isPc;
    [SerializeField] private InputActionAsset controls;

    Animator rayAnim;
    [SerializeField] private Transform firePoint;
    private bool isFreezed;
    int delayCount = 2;

    [SerializeField] private GameObject door;

    private void Start()
    {
        rayAnim = GetComponentInChildren<Animator>();
        isFreezed = false;
    }

    private void OnEnable()
    {
        controls.FindActionMap("Player").FindAction("Move").performed += OnMove;
        controls.FindActionMap("Player").FindAction("Move").canceled += OnMoveCancled;
        controls.FindActionMap("Player").FindAction("mouselook").performed += OnMouseLook;

    }

    private void OnDisable()
    {

        controls.FindActionMap("Player").FindAction("Move").performed -= OnMove;
        controls.FindActionMap("Player").FindAction("Move").canceled -= OnMoveCancled;
        controls.FindActionMap("Player").FindAction("mouselook").performed -= OnMouseLook;
    }

    private void OnMoveCancled(InputAction.CallbackContext obj)
    {
        move = Vector2.zero;
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        move = context.ReadValue<Vector2>();

        Debug.Log("Moving");
    }



    public void OnMouseLook(InputAction.CallbackContext context)
    {
        mouseLook = context.ReadValue<Vector2>();
    }

    public void OnJoyStickLook(InputAction.CallbackContext context)
    {
        joyStickLook = context.ReadValue<Vector2>();


    }


    public void MovePlayerWithAim()
    {
        if (isPc)
        {
            var lockPos = rotationTarget - transform.position;
            lockPos.y = 0;
            var rotation = Quaternion.LookRotation(lockPos);

            Vector3 aimDirection = new Vector3(rotationTarget.x, 0f, rotationTarget.z);
            if (aimDirection != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.15f);
            }
            //
            firePoint.LookAt(aimDirection);
        }
        else
        {
            Vector3 aimDirection = new Vector3(joyStickLook.x, 0f, joyStickLook.y);
            if (aimDirection != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(aimDirection), 0.15f);
            }
            //
            firePoint.LookAt(aimDirection);

        }
        Vector3 movment = new Vector3(move.x, 0f, move.y);
        transform.Translate(movment * speed * Time.deltaTime, Space.World);


    }



    // Update is called once per frame
    void Update()
    {
        Moving();
        if (isPc)
        {
            RaycastHit hit;
            Ray ray = mainCamera.ScreenPointToRay(mouseLook);
            if (Physics.Raycast(ray, out hit))
            {
                rotationTarget = hit.point;

            }
            MovePlayerWithAim();
        }
        else
        {

            if (joyStickLook.x == 0 && joyStickLook.y == 0)
            {
                Moving();
            }
            else
            {
                MovePlayerWithAim();
            }

        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            FreezeTime();
        }

    }
    void Moving()
    {
        Vector3 movment = new Vector3(move.x, 0f, move.y);
        rayAnim.SetFloat("Moving", movment.magnitude);
        if (movment != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movment), 0.15f);
        }
        if (isFreezed) speed = 12f;
        transform.Translate(movment * speed * Time.deltaTime, Space.World);
    }
    public void FreezeTime()
    {
        
        isFreezed = true;
        StartCoroutine(FreezeDelay());
    }
    IEnumerator FreezeDelay()
    {
        Time.timeScale = 0.3f;
        yield return new WaitForSeconds(delayCount);
        Time.timeScale = 1f;
        isFreezed = false;
        speed = 6.89f;
    }
    




}
