using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ameraFollow : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float smoothtime;
    [SerializeField] Vector3 offset;
    [SerializeField] Vector3 Velocity = Vector3.zero;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            Vector3 targetPos = target.position + offset;
            transform.position = Vector3.SmoothDamp(transform.position, target.position, ref Velocity, smoothtime);
        }
    }
}
