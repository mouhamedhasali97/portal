using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleEnemy : MonoBehaviour
{
    [SerializeField] float radius = 10f;
    [SerializeField] float speed = 1f;

    private float angle = 0f;
    private void Start()
    {
        Vector2 currentPosition = new Vector2(transform.position.x, transform.position.z);
        angle = Mathf.Atan2(currentPosition.y, currentPosition.x);
    }

    private void Update()
    {
        angle += speed * Time.deltaTime;

        float x = radius * Mathf.Cos(angle);
        float z = radius * Mathf.Sin(angle);

        transform.position = new Vector3(x, transform.position.y, z);
    }
}
