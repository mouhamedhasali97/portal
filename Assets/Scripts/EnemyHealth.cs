using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] GameObject DarkMatterDeathEffect;
    [SerializeField] GameObject DarkMatterDamageEffect;
    [SerializeField] float enemyMaxHealth;
    float currentHealth;
    // Start is called before the first frame update
    void Start()
    {

        currentHealth = enemyMaxHealth;
    }

    public void AddDamage(float damage)
    {
        Instantiate(DarkMatterDamageEffect,transform.position,Quaternion.identity);
        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        Instantiate(DarkMatterDeathEffect,transform.position,Quaternion.identity);
        Destroy(gameObject);
    }
}
