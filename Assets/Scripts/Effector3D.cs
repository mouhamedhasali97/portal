using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effector3D : MonoBehaviour
{
    [SerializeField] float radius;
    [SerializeField] float force;
    

    

    // Update is called once per frame
    void Update()
    {
        Vector3 effectorPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(effectorPos,radius);
        foreach(Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if(rb!= null)
            {
                rb.AddExplosionForce(force * Time.deltaTime, effectorPos, radius);
            }
        }
    }
}
