using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshObstacle))] 
public class DoorOpen : MonoBehaviour
{
    private NavMeshObstacle obstacle;
    public bool isOpen = false;
    [SerializeField] float speed = 1f;
    [SerializeField] float RotationAmount = 90f;
    [SerializeField] float forwardDirection = 0f;

    private Vector3 StartRotation;
    private Vector3 Forward;


    private Coroutine AnimationCoroutine;


    private void Awake()
    {
        obstacle = GetComponent<NavMeshObstacle>();
        obstacle.carveOnlyStationary = false;
        obstacle.carving = true;
        obstacle.enabled = true;
        StartRotation = transform.rotation.eulerAngles;
        Forward = transform.up;


    }
    public void Open(Vector3 PlayerPosition)
    {
        if (!isOpen)
        {
            if(AnimationCoroutine != null)
            {
                StopCoroutine(AnimationCoroutine);

            }
            float dot = Vector3.Dot(Forward, (PlayerPosition - transform.position).normalized);
            AnimationCoroutine = StartCoroutine(DoRotaionOpen(dot));


        }
    }
    IEnumerator DoRotaionOpen(float ForwardAmount)
    {
        Quaternion startRotation = transform.rotation;
        Quaternion endRotation;
        if(ForwardAmount >= forwardDirection)
        {
            endRotation = Quaternion.Euler(new Vector3(StartRotation.x,StartRotation.y - RotationAmount,StartRotation.z));

        }
        else
        {
            endRotation = Quaternion.Euler(new Vector3(StartRotation.x,StartRotation.y + RotationAmount,StartRotation.z));

        }
        isOpen = true;
        float time = 0;
        while (time < 1)
        {
            transform.rotation = Quaternion.Slerp(startRotation, endRotation, time);
            yield return null;
            time += Time.deltaTime * speed;
        }
        obstacle.enabled = true;
        obstacle.carving = true;


    }
    public void Close()
    {
        if (isOpen)
        {
            if (AnimationCoroutine != null)
            {
                StopCoroutine(AnimationCoroutine);

            }
            AnimationCoroutine = StartCoroutine(DoRotaionClose());


        }

    }
    IEnumerator DoRotaionClose()
    {
        obstacle.carving = false;
        obstacle.enabled = false;

        Quaternion startRotation = transform.rotation;
        Quaternion endRotation = Quaternion.Euler(StartRotation);
        isOpen = false;
        float time =0 ;
        while (time < 1)
        {
            transform.rotation = Quaternion.Slerp(startRotation, endRotation, time);
            yield return null;
            time += Time.deltaTime * speed;
        }
    }





    




}
