using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Weapon : MonoBehaviour
{
    [SerializeField] Transform shootTip;
    [SerializeField] GameObject lightBulletPrefab;
    [SerializeField] private float lightBulletForce =20f;
    private InputManager playerControls;
    private InputAction attack;
    Animator rayAnim;
    private void Awake()
    {
        playerControls = new InputManager();
        rayAnim = GetComponentInChildren<Animator>();
    }
    private void OnEnable()
    {
        attack = playerControls.Player.Attack;
        attack.Enable();
        attack.performed += Attack; 
    }
    private void OnDisable()
    {
        playerControls.Disable();
        attack.Disable();
    }
    public void Attack(InputAction.CallbackContext context)
    {
        if (Time.timeScale >= 1f)
                StartCoroutine(Fire());
    }
    IEnumerator Fire()
    {
       GameObject lightBullet =  Instantiate(lightBulletPrefab, shootTip.position, shootTip.rotation);
       Rigidbody rb =  lightBullet.GetComponent<Rigidbody>();
        rb.AddForce(shootTip.transform.forward * lightBulletForce);
       yield return new WaitForSeconds(5f);
        Destroy(lightBullet);
    }




}