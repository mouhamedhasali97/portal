using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] int sceneIndex;
    [SerializeField] float delayTime = 1.0f; 

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Invoke("LoadSceneWithDelay", delayTime);
        }
    }

    private void LoadSceneWithDelay()
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
