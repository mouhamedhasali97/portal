using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField] float forceMagnitude = 10f;
    [SerializeField] float movementDuration = 0.5f;

    Animator darkAnim;

    [SerializeField] NavMeshAgent agent;
    Transform player;
    [SerializeField] LayerMask whatIsGround, whatIsplayer;
    [SerializeField] Vector3 walkPoint;
    bool walkPointSet;
    [SerializeField] float walkpointRange;

    [SerializeField] float timeBetweenAttacks;
    bool alreadyAttacked;

    [SerializeField] float sightRange, attackRange;
    [SerializeField] bool playerInSightRange, playerInAttackRange;

    [SerializeField] float damageThePlayer = 50f;
    private void Awake()
    {
        darkAnim = GetComponentInChildren<Animator>();
        player = FindObjectOfType<Player>().transform;
        agent = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsplayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsplayer);
        if(!playerInAttackRange && !playerInSightRange)
        {
            Patroling();
        }
        if(playerInSightRange && !playerInAttackRange)
        {
            ChasePlayer();
        }
        if(playerInSightRange && playerInAttackRange)
        {
            AttackPlayer();
        }
    }
    void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();
        if (walkPointSet) agent.SetDestination(walkPoint);
        Vector3 distanceToWalkPoint = transform.position - walkPoint;
        if(distanceToWalkPoint.magnitude <1f)
        {
            walkPointSet = false;
        }
    }
    void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkpointRange, walkpointRange);
        float randomx = Random.Range(-walkpointRange, walkpointRange);
        walkPoint = new Vector3(transform.position.x + randomx , transform.position.y, transform.position.z + randomZ);
        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
        {
            walkPointSet = true; 
        }    
    }
    void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }
    void AttackPlayer()
    {
        agent.SetDestination(player.position);
        transform.LookAt(player);
        if (!alreadyAttacked)
        {
          
            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }

    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        darkAnim.SetTrigger("Kill");
        if(other.gameObject.tag == "Player")
        {
            
            Rigidbody playerRigidbody = other.gameObject.GetComponent<Rigidbody>();
            PlayerHealth rayHealth = other.gameObject.GetComponent<PlayerHealth>();
            rayHealth.AddDamage(damageThePlayer);
            
              


        }
    }


}
